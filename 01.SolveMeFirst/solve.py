# Author: Md. Fahim Bin Amin (https://www.fahimbinamin.com)

# URL of the problem: https://www.hackerrank.com/challenges/solve-me-first/problem

def solveMeFirst(a,b):
	# Hint: Type return a+b below
    return (a+b) # returning the addition value of a and b


num1 = int(input())
num2 = int(input())
res = solveMeFirst(num1,num2)
print(res)
